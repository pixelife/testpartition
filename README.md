# Utilisation

First clone the repo

## To use directly and edit with your favorite IDE
Use it's **import maven** functionnality

```java
    /**
     *  Exemple of usage
     */
    import com.adneom.recrutement.javatest.SomeUtils;
    import java.util.Arrays;
    
    public class ExampleClass {
        public static void main(String[] args) {
            // flat array
            String[] entries = new String[] { "a", "b", "c", "d", "e", "f", "g" };
            
            // flat arraylist
            List<String> listEntries = Arrays.asList(entries);
            
            try {
                // partitionned array
                String[][] partEntries = (String[][])SomeUtils.partition(entries,3);
                System.out.println(Arrays.deepToString(partEntries));
                // partitionned list
                List<List<String>> partListEntries = (List<List<String>>)SomeUtils.partition(listEntries,3);
                System.out.println(partListEntries);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
```


# To create a jar archive ( and execute bundled unit tests)

With your system console :
```sh
# change dir to path of cloned repo
$ cd path/to/the/cloned/repo
# use maven to package the project
$ mvn package

# some maven output will be displayed 

# if everything went fine, maven will output the jar in target/TestPartition-1.0.0.jar
# to confirm
$ ls target/*jar
```

The jar is ready to be used in your dependencies
