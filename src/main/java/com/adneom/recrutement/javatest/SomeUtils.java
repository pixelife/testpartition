package com.adneom.recrutement.javatest;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @author A. Adam
 * 
 *         Some utils function
 * 
 */
public class SomeUtils {

	/**
	 * You may need to cast the result into its original type
	 * 
	 * @param liste
	 *            take a list of anything
	 * @param taille
	 *            size of sub lists
	 * @return a list of sub lists with a max size of taille containing elements of
	 *         previous list with the same ordering
	 * @throws Exception
	 *             if size is greater than 0, an exception is thrown
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<?> partition(List<?> liste, int taille) throws Exception {
		if (taille <= 0) {
			throw new Exception("Invalid parameter value for 'taille', 'taille' must be greater than 0");
		}

		// Calculate final list size
		int finalListSize = (int) Math.ceil(liste.size() / (double) taille);

		// Initialize result
		List<List> res = new ArrayList<List>(finalListSize);

		for (int i = 0; i < finalListSize; i++) {
			res.add(new ArrayList<Object>(taille));
		}

		// store elements in sub lists
		for (int i = 0, listSize = liste.size(); i < listSize; i++) {
			res.get(i / taille).add(liste.get(i));
		}

		return res;

	}

	/**
	 * You may need to cast the result into its original type
	 * 
	 * @param liste
	 *            : take a list of anything
	 * @param taille
	 *            : size of sub lists
	 * @return a list of sub lists with a max size of taille containing elements of
	 *         previous list with the same ordering
	 * @throws Exception
	 *             if size is greater than 0, an exception is thrown
	 */
	@SuppressWarnings("unchecked")
	public static Object[][] partition(Object liste, int taille) throws Exception {
		Object[] flatlist = (Object[]) liste;
		if (taille <= 0) {
			throw new Exception("Invalid parameter value for 'taille', 'taille' must be greater than 0");
		}

		// Calculate final list size
		int finalLength = (int) Math.ceil(flatlist.length / (double) taille);
		
		// Initialize result
		Object[][] res = (Object[][]) Array.newInstance(liste.getClass(), finalLength);
		
		List<Object>[] intermediary = new List[finalLength]; 

		for (int i = 0; i < finalLength; i++) {
			intermediary[i] = new ArrayList<Object>(taille);
		}

		// store elements in sub lists
		for (int i = 0, arrLength = flatlist.length; i < arrLength; i++) {
			List<Object> subList = intermediary[i/taille];
			subList.add(flatlist[i]);
		}

		// reconvert all sublist to fixed length array
		for (int i = 0; i < finalLength; i++) {
			// we have to hack a bit to get the proper type to be stored in the array ...
			Object[] dummyObjForType = (Object[])Array.newInstance(flatlist[0].getClass(), intermediary[i].size());
			res[i] = intermediary[i].toArray(dummyObjForType);
		}

		return res;

	}

}
