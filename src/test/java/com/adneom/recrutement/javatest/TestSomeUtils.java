package com.adneom.recrutement.javatest;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Some JUnit tests for class com.adneom.recrutement.javatest.SomeUtils 
 * @author A. Adam
 *
 */
public class TestSomeUtils {
	private Integer[] dataExerciseArray;
	private List<Integer> dataExerciseList;
	List<List<Integer>> expectedList1, expectedList2, expectedList3;
	Integer[][] expectedArray1, expectedArray2, expectedArray3;

	/**
	 * Setup expected data and input
	 */
	@Before
	public void setup() {
		dataExerciseArray = new Integer[5];
		dataExerciseList = new ArrayList<>();
		for (int i = 0; i < dataExerciseArray.length; i++) {
			dataExerciseArray[i] = i + 1;
			dataExerciseList.add(i + 1);
		}

		// expectedList1 = [ [1,2], [3,4], [5] ]
		expectedList1 = new ArrayList<List<Integer>>();
		for (int i = 0; i < 3; i++)
			expectedList1.add(new ArrayList<Integer>());

		expectedList1.get(0).add(1);
		expectedList1.get(0).add(2);
		expectedList1.get(1).add(3);
		expectedList1.get(1).add(4);
		expectedList1.get(2).add(5);

		// expectedList2 = [ [1,2,3], [4,5] ]
		expectedList2 = new ArrayList<>(2);
		for (int i = 0; i < 2; i++)
			expectedList2.add(new ArrayList<Integer>());

		expectedList2.get(0).add(1);
		expectedList2.get(0).add(2);
		expectedList2.get(0).add(3);
		expectedList2.get(1).add(4);
		expectedList2.get(1).add(5);

		// expectedList3 = [ [1], [2], [3], [4], [5] ]
		expectedList3 = new ArrayList<>(5);
		for (int i = 0; i < 5; i++)
			expectedList3.add(new ArrayList<Integer>());

		expectedList3.get(0).add(1);
		expectedList3.get(1).add(2);
		expectedList3.get(2).add(3);
		expectedList3.get(3).add(4);
		expectedList3.get(4).add(5);

		// expectedArray1 = [ [1,2], [3,4], [5] ]
		expectedArray1 = new Integer[3][];
		expectedArray1[0] = new Integer[2];
		expectedArray1[1] = new Integer[2];
		expectedArray1[2] = new Integer[1];
		expectedArray1[0][0] = 1;
		expectedArray1[0][1] = 2;
		expectedArray1[1][0] = 3;
		expectedArray1[1][1] = 4;
		expectedArray1[2][0] = 5;

		// expectedArray2 = [ [1,2,3], [4,5] ]
		expectedArray2 = new Integer[2][];
		expectedArray2[0] = new Integer[3];
		expectedArray2[1] = new Integer[2];
		expectedArray2[0][0] = 1;
		expectedArray2[0][1] = 2;
		expectedArray2[0][2] = 3;
		expectedArray2[1][0] = 4;
		expectedArray2[1][1] = 5;

		// expectedArray3 = [ [1], [2], [3], [4], [5] ]
		expectedArray3 = new Integer[5][];
		for (int i = 0; i < expectedArray3.length; i++) {
			expectedArray3[i] = new Integer[1];
			expectedArray3[i][0] = i + 1;
		}
	}

	/**
	 * Test if taille > 0
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_partition_list_0() throws Exception {
		try {
			SomeUtils.partition(dataExerciseList, -1);
		} catch (Exception e) {
			return;
		}
		fail("Test failure for argument 'taille' in partition()");
	}

	/**
	 * partition([1,2,3,4,5], 2) retourne: [ [1,2], [3,4], [5] ]
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void test_partition_list_1() throws Exception {
		List<List<Integer>> result = (List<List<Integer>>) SomeUtils.partition(dataExerciseList, 2);
		assertEquals(expectedList1, result);
		assertEquals(expectedList1.size(), result.size());
	}

	/**
	 * partition([1,2,3,4,5], 3) retourne: [ [1,2,3], [4,5] ]
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void test_partition_list_2() throws Exception {
		List<List<Integer>> result = (List<List<Integer>>) SomeUtils.partition(dataExerciseList, 3);
		assertEquals(expectedList2, result);
		assertEquals(expectedList2.size(), result.size());
	}

	/**
	 * partition([1,2,3,4,5], 1) retourne: [ [1], [2], [3], [4], [5] ]
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void test_partition_list_3() throws Exception {
		List<List<Integer>> result = (List<List<Integer>>) SomeUtils.partition(dataExerciseList, 1);
		assertEquals(expectedList3, result);
		assertEquals(expectedList3.size(), result.size());
	}

	/**
	 * Test if taille > 0
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_partition_array_0() throws Exception {
		try {
			Integer[][] result = (Integer[][]) SomeUtils.partition(dataExerciseArray, 0);
		} catch (Exception e) {
			return;
		}
		fail("Test failure for argument 'taille' in partition()");
	}

	/**
	 * partition([1,2,3,4,5], 2) retourne: [ [1,2], [3,4], [5] ]
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_partition_array_1() throws Exception {
		Integer[][] result = (Integer[][]) SomeUtils.partition(dataExerciseArray, 2);
		assertArrayEquals(expectedArray1, result);
		assertEquals(expectedArray1.length, result.length);
	}

	/**
	 * partition([1,2,3,4,5], 3) retourne: [ [1,2,3], [4,5] ]
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_partition_array_2() throws Exception {
		Integer[][] result = (Integer[][]) SomeUtils.partition(dataExerciseArray, 3);
		assertArrayEquals(expectedArray2, result);
		assertEquals(expectedArray2.length, result.length);
	}

	/**
	 * partition([1,2,3,4,5], 1) retourne: [ [1], [2], [3], [4], [5] ]
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_partition_array_3() throws Exception {
		Integer[][] result = (Integer[][]) SomeUtils.partition(dataExerciseArray, 1);
		assertArrayEquals(expectedArray3, result);
		assertEquals(expectedArray3.length, result.length);
	}

}
